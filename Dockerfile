# werckerビルド環境: MySQL
FROM mysql:@ver@
MAINTAINER Technosite Cloud

# mysql
COPY charset.cnf /etc/mysql/conf.d/
COPY bind-address.cnf /etc/mysql/conf.d/

RUN apt-get -y update; \
    apt-get -y upgrade; \
    apt-get -y install \
        apt-utils \
        locales \
        locales-all; \
    # Clean up
    apt-get autoremove -y; \
    apt-get clean -y; \
    rm -rf /var/lib/apt/lists/*; \
    sed -i -e "s/^# ja_JP/ja_JP/" /etc/locale.gen; \
    locale-gen; \
    update-locale LANG=ja_JP.UTF-8 LANGUAGE=ja_JP:ja; \
    cat /etc/mysql/my.cnf; \
    # Authority
    chmod 644 /etc/mysql/conf.d/charset.cnf /etc/mysql/conf.d/bind-address.cnf
ENV LANG ja_JP.UTF-8
ENV LC_ALL ja_JP.UTF-8
ENV LC_CTYPE ja_JP.UTF-8

