# mysql-utf8
MySQL(UTF8)

- ビルドステータス

| master | develop |
| ------ | ------ |
| [![pipeline status](https://gitlab.com/tsc-base/mysql-utf8/badges/master/pipeline.svg)](https://gitlab.com/tsc-base/mysql-utf8/-/commits/master) | [![pipeline status](https://gitlab.com/tsc-base/mysql-utf8/badges/develop/pipeline.svg)](https://gitlab.com/tsc-base/mysql-utf8/-/commits/develop) |

- コミット/プッシュ時に自動ビルドされる
  - コンテナイメージのビルドのみ実行する
- 毎月1日 0:00に、ビルドがスケジュール実行される
  - ベースイメージ:[MySQL](https://hub.docker.com/_/mysql)が更新されていたら、コンテナイメージを再ビルドする
